//
//  Model.swift
//  WeatherApp
//
//  Created by BOND on 05/10/21.
//

import Foundation

//Citywise get weather details model
struct weatherDetails {
    
    var main:String
    var description:String
    var id:Int
    var icon:String
    
    init(json: [String:Any]) {
        self.main = json["main"] as? String ?? ""
        self.description = json["description"] as? String ?? ""
        self.id = json["id"] as? Int ?? 0
        self.icon = json["icon"] as? String ?? ""
    }
}
