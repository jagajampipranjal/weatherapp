//
//  APIManager.swift
//  WeatherApp
//
//  Created by BOND on 05/10/21.
//

import Foundation


class APIManager
{
    let baseURL = "https://api.openweathermap.org"
    let appID = "33dedfa14ce8895b5420baa1d49405d0"
    
    static let sharedInstance = APIManager()
    
    //API to get citywise weather details
    func apiGetWeatherDetails(city:String,
                  onsuccess:@escaping ([weatherDetails], Double, Int, String) -> Swift.Void,
                  onfailure:@escaping(String?)->Void)
    {
        
        
        guard let url = URL(string: "\(baseURL)/data/2.5/weather?q=\(city)&appid=\(appID)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) else { return }
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        
        print(url)
        
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) in
            DispatchQueue.main.async {
                
            }
            if error != nil {
                print(error ?? "")
                onfailure("Check your internet connection!")
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String:Any]
                print(json)
                
                let success = json["cod"] as? Int
                if(success == 200){
                    
                    let temparray = json["weather"] as? [[String:Any]]
                    if let temparray = temparray
                    {
                        var all:[weatherDetails] = []
                        for dict1 in temparray {
                            let dic = weatherDetails(json: dict1)
                            all.append(dic)
                            
                        }
                        
                        let main = json["main"] as? [String:Any]
                        let temp = main?["temp"] as? Double ?? 0.0
                        let humidity = main?["humidity"] as? Int ?? 0
                        
                        let name = json["name"] as? String ?? ""
                        
                        onsuccess(all, temp, humidity, name)
                    }
                    else{
                        
                        onsuccess([], 0.0, 0, "")
                    }
                  
                    
                }
                else
                {
                    onfailure("City not found")
                }
            } catch let jsonError {
                print(jsonError)
                onfailure("Something went wrong")
                
            }
        }).resume()
        
        
    }
}
