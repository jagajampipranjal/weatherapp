//
//  ViewController.swift
//  WeatherApp
//
//  Created by BOND on 04/10/21.
//

import UIKit
import CoreData

class ViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var weatherSearchBar: UISearchBar!
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var weatherLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var humidityLbl: UILabel!
    
    var all = [weatherDetails] ()
    var apiResponse = false
    var weatherData: [NSManagedObject] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weatherSearchBar.delegate = self
        
        // Do any additional setup after loading the view.
        
    }

    // Call citywise get weather data API
    func getWeatherData(city:String) {
        
        APIManager.sharedInstance.apiGetWeatherDetails(city:city) {[weak self] (all,temp,humidity,name)  in
            
            DispatchQueue.main.async {
                
                self?.all = all
                if(self?.all.count != 0) {
                    
                    let desc = self?.all[0].description ?? ""
                    let weather = self?.all[0].main ?? ""
                    
                    self?.saveWeatherDetails(temp: temp, humadity: humidity, name: name, desc:desc, weather: weather)
                    self?.setWeatherData(temp: temp, humadity: humidity, name: name, desc:desc, weather: weather)
                }
            }
        } onfailure: { (msg) in
            
            DispatchQueue.main.async {
                
                if let msg = msg{
                    self.addalert(on: self, title: "Alert", subtitle: msg)
                }
            }

        }
    }
    
    //Display citywise Weather details on UI
    func setWeatherData(temp:Double, humadity:Int, name:String, desc:String, weather:String) {
        
            self.cityNameLbl.text = name
            self.descLbl.text = desc
            self.humidityLbl.text = String(humadity)
            self.weatherLbl.text = weather
            self.tempLbl.text = String(temp)
        
    }
    
    //MARK:- Core data methods
    
    //save new citywise weather details in local database
    func saveWeatherDetails(temp:Double, humadity:Int, name:String, desc:String, weather:String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "WeatherEntity", in: managedContext)!
        
        let weatherData = NSManagedObject(entity: entity, insertInto: managedContext)
        
        weatherData.setValue(name, forKeyPath: "name")
        weatherData.setValue(desc, forKeyPath: "desc")
        weatherData.setValue(humadity, forKeyPath: "humadity")
        weatherData.setValue(temp, forKeyPath: "temp")
        weatherData.setValue(weather, forKeyPath: "weather")
        
        do {
            try managedContext.save()
            print("weather details saved successfully")
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    //Fetch citywise weather details from local database
    func fetchData(cityName:String) -> Bool {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
          
        let managedContext = appDelegate.persistentContainer.viewContext
          
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WeatherEntity")
        fetchRequest.predicate = NSPredicate(format: "name = %@", cityName)
        
        do {
            
            weatherData = try managedContext.fetch(fetchRequest)
            
            for weatherDetails in weatherData {
                
                let temp = weatherDetails.value(forKeyPath: "temp") as? Double ?? 0.0
                let humadity = weatherDetails.value(forKeyPath: "humadity") as? Int ?? 0
                let name = weatherDetails.value(forKeyPath: "name") as? String ?? ""
                let desc = weatherDetails.value(forKeyPath: "desc") as? String ?? ""
                let weather = weatherDetails.value(forKeyPath: "weather") as? String ?? ""
                self.setWeatherData(temp: temp, humadity: humadity, name: name, desc: desc, weather: weather)
                print("fetched weather data successfully")
                
                return true
            }
            
        } catch let error as NSError {
            
            print("Could not fetch. \(error), \(error.userInfo)")
            
          }
        return false
    }
    

    
    //MARK:- SearchBar Delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        print("searchText \(searchBar.text ?? "")")
        
        if searchBar.text != "" {
            
            let fetchBool = self.fetchData(cityName: searchBar.text ?? "")
            
            if fetchBool == false {
                self.getWeatherData(city:searchBar.text ?? "")
            }
        }
        
    }
}

//MARK:- extension UIViewController
extension UIViewController
{
   
    func addalert(on vc:UIViewController ,title:String,subtitle:String)
    {
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    
}
